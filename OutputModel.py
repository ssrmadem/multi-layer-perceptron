import numpy as np
import matplotlib.pyplot as plt
## Creating a Class - MLP
class MLP():
    
## The init function initializes the variables InputSize, Learning Rate and the number of Epochs that need to be run.

    def __init__(self, InputSize = 1, LearningRate = 0.2, Momentum = 0, Epochs= 10):
        self.InputSize = InputSize
        self.LearningRate = LearningRate
        self.Momentum = Momentum
        self.Epochs = Epochs
        self.Layers = []
        self.Input = []
    
## Add Layer function Adds a new Layer with the number of neurons in that layer and the layerType (Hidden or Output)
    
    def AddLayer(self, NumNeurons, LayerType= 'hidden'):
        ##print(LayerType)
        layer = Layer(NumNeurons,LayerType)
        self.Layers.append(layer)
    
    ## Initializing Weight Matrices for each Layer
    
    def InitializeWeights(self):
        InpSize = self.InputSize
        for i in range(len(self.Layers)):
            InpSize = self.Layers[i].InitializeLayerWeights(InpSize)
    
    ## Feed Forward function which has Sigmoid as the activation function at the hidden layer and Softmax as the activation function at the output layer
    
    def ForwardPass(self, X):
        self.Input = X
        for j in range(len(self.Layers)):
            self.Input = self.Layers[j].LayerOutput(self.Input)
            ##print("FinalOutput",self.Input)
        return self.Input
    
    ## Error Calculation - Here I am using the cross-entropy loss function to calculate the loss. i.e. Sum(y_expected*log(y_output))
    
    def CalculateError(self,Output, Expected):
        L_sum = np.sum(np.multiply(Expected, np.log(Output+np.exp(-8))))
        m = len(Output)
        L = -(1/m) * L_sum
        return L

    ## Back Propogation function that calculates the delta weights and also updating tdhe weight matrices for each layer

    def BackPropogation(self, y):
        self.DeltaWeights(y)
        ##Update Weights
        self.UpdateWeights(self.LearningRate)

    ## Update Weights function to update the weights for each layer

    def UpdateWeights(self, Rate):
        for i in range(len(self.Layers)):
            self.Layers[i].UpdateLayerWeights(Rate)

    ## Calculating the back Propogated error for each layer

    def DeltaWeights(self, y):
        temp = y
        LayerDel = []
        NextWeight = []
        ## Calculating Error Signal that will be propogated for all layers
        for i in range(len(self.Layers)):
            inp, Delta, DeltaWeight  = self.Layers[len(self.Layers) - i - 1].CalculateDeltaWeights(temp, LayerDel, NextWeight)
            temp = inp
            LayerDel = Delta
            NextWeight = DeltaWeight

    ## Fit method Trains the data and Reports the average cross entropy loss for each Epoch and also divides the data into train and validation set, the train set is used to train the data to update the weights and the validation data is used to predict the output and report the accuracy after each epoch in order to avoid overfitting of the dataset.

    def fit(self, X, y):
        np.reshape(X, (-1, 784))
        Error = 0

        self.prevValAcc = 0
        self.ValAcc = 0

    ## For Each Epoch we loop through all the values of the train Data and train the network
        self.GraphplotTrainAcc = []
        self.GraphplotValAcc = []
        self.GraphplotTrainLoss = []
        self.GraphplotValLoss = []
        for i in range(self.Epochs):
            randomize = np.arange(len(X))
            np.random.shuffle(randomize)
            X = X[randomize]
            y = y[randomize]
            ## Dividing the Data into Train and Test Dataset in the ratio 80:20
            Divide = int(len(X) * 0.8)
            X_TraData = X[0:Divide]
            X_ValData = X[Divide + 1:]
            y_TraData = y[0:Divide]
            y_ValData = y[Divide + 1:]
            sigmaError = 0
            self.y_ValPredict = []
            self.y_TraPredict = []
            self.Arr = []
            for j in range(len(X_TraData)):
                Output = self.ForwardPass(X_TraData[j])
                self.BackPropogation(y_TraData[j])
                last = y_TraData[j]
                Error = self.CalculateError(Output, last)
                sigmaError += Error
            AverageError = sigmaError/len(X_TraData)

            ## After each Epoch, using Train Data and Validation Data, the y_labels are predicted and the accuracy is calculated and reported, the average Cross-Entropy Loss is also reported for each Epoch.
            self.y_TraPredict = self.Predict(X_TraData)
            self.y_ValPredict = self.Predict(X_ValData)
            sigmaValError = 0
            for k in range(len(self.y_ValPredict)):
                ValError = self.CalculateError(self.y_ValPredict[k], y_ValData[k])
                sigmaValError += ValError
            AverageValError = sigmaValError/len(self.y_ValPredict)
            self.prevValAcc = self.ValAcc
            self.TraAcc = self.CalcAccuracy(self.y_TraPredict, y_TraData)
            self.ValAcc = self.CalcAccuracy(self.y_ValPredict, y_ValData)
            self.GraphplotTrainAcc.append(self.TraAcc)
            self.GraphplotValAcc.append(self.ValAcc)
            self.GraphplotTrainLoss.append(AverageError)
            self.GraphplotValLoss.append(AverageValError)
            print ("Epoch", i+1,"\n\t", "Average Training Cross Entropy Loss = ", AverageError, "\n\t", "Train Accuracy = ", self.TraAcc, "\n\t", "Validation Accuracy = ", self.ValAcc, "\n\t", "Average Cross Entropy loss Validation = ", AverageValError)
            ## The Training stops if the Validation Accuracy starts to reduce
            # if (self.ValAcc < self.prevValAcc):`
            #     break

    ## Code for generating graphs

        # plt.plot(self.GraphplotTrainAcc, label="TrainAccuracy")
        # plt.plot(self.GraphplotValAcc, label="ValidationAccuracy")
        # plt.xlabel('Number Of Epochs')
        # plt.ylabel('Accuracy')
        # plt.legend()
        # plt.show()
        # plt.plot(self.GraphplotTrainLoss, label="TrainLoss")
        # plt.plot(self.GraphplotValLoss, label="ValidationLoss")
        # plt.xlabel('Number Of Epochs')
        # plt.ylabel('Cross Entropy Loss')
        # plt.legend()
        # plt.show()
    ## The predict function outputs the prediction of the output for a given set of weights
    def Predict(self,X):
        self.FinalPred = []
        tempo = 0
        for i in range(len(X)):
            PredOutput = self.ForwardPass(X[i])
            ##print("pred out", PredOutput)
            tempo = np.argmax(PredOutput)
            pred = np.zeros(len(PredOutput))
            pred[tempo] = 1
            ##print("hi",pred)
            self.FinalPred.append(pred)
        self.FinalPred = np.asarray(self.FinalPred) 
        return self.FinalPred

    ## The accuracy function calculates the accuracy of the predicted Data by comparing it with the test data labels

    def CalcAccuracy(self, y_hat, y):
        sum = 0
        for i in range(len(y_hat)):
            y_hat_max = np.argmax(y_hat[i])
            y_max = np.argmax(y[i])
            if (y_hat_max == y_max):
                sum += 1
        accuracy = sum/len(y_hat)
        return accuracy

## Layer Class is created for each layer that is added

class Layer():
    def __init__(self,NumNeurons, LayerType= 'hidden'):
        self.neurons = []
        self.LayerType = LayerType
        self.NumNeurons = NumNeurons
        self.LayerWeightMatrix = []
        self.LayerOutArray = []
        self.LayerDelta = []
        self.InputArr = []
        self.InputMatrix = []
        self.OutputMatrix = []
        self.LayerDelta = []
        self.DeltaWeight = []
        for i in range(self.NumNeurons):
            neuron = Neuron(i, self.LayerType)
            self.neurons.append(neuron)
    def InitializeLayerWeights(self, NumInputs):
        ##self.LayerWeightMatrix = np.random.rand(NumInputs+1, len(self.neurons))
        self.LayerWeightMatrix = np.full((NumInputs+1,len(self.neurons)), 0)
        ##print(self.LayerWeightMatrix)
        return len(self.neurons)
    def LayerOutput(self, InpMatrix):
        ##print("layer ", self.LayerType)
        self.LayerWeightMatrix = np.asarray(self.LayerWeightMatrix) 
        self.InputMatrix = InpMatrix
        self.InputMatrixBias =np.append(self.InputMatrix,[-1])
        self.OutputArray = np.matmul(np.transpose(self.LayerWeightMatrix), np.transpose(self.InputMatrixBias))
        ##print("norm",self.OutputArray)

    ## In forward Pass, Sigmoid Activation function is used for the hidden layers and Softmax Activation Function is used for the Output Layer

        self.OutputArray = self.Activation(self.OutputArray)
        ##print("after",self.OutputArray)
        return self.OutputArray
    def Activation(self, Output):
        if(self.LayerType == 'hidden'):
            activation_output = self.Sigmoid(Output)
        else:
            activation_output = self.Softmax(Output)
        return activation_output
    def Sigmoid(self, tot):
        Out = 1 / (1 + np.exp(-tot))
        return Out
    def Softmax(self, tot):
        e = np.exp(tot)
        p = e/np.sum(e, axis=0)
        return p
    def SigmoidDerivative(self, tot):
        return tot*(1-tot)

    ## This function Calculates the DeltaWeights of each layer in the Back Propogation stage

    def CalculateDeltaWeights(self, y, LayerDel, nextWeight):
        self.InputMatrix = self.InputMatrix.reshape(-1, len(self.InputMatrix))
        self.InputMatrixBias = self.InputMatrixBias.reshape(-1, len(self.InputMatrixBias))
        if(self.LayerType == 'output'):
            ## To find the back propogated error at the output layer we use derivative of the cross Entropy function multiplied by the derivative of Softmax Function, this gives (Output - Expected) as result
            self.LayerDelta = self.OutputArray - y
            self.LayerDelta = self.LayerDelta.reshape(-1, len(self.LayerDelta))
            ## The Delta Weight of Hidden to Output Layer is Caluclated by multiplying the backpropogated error with the output of the Hidden Layer
            self.DeltaWeight = np.matmul(np.transpose(self.LayerDelta) , self.InputMatrixBias)
            self.DeltaWeight = np.transpose(self.DeltaWeight)
        else:
            ## To find the back propogated error for the hidden Layers - sum(BackPropError_output * Weight_hidden_output * output_hidden * (1- output_hidden))
            ## Here output_hidden * (1- output_hidden) is the derivative of the sigmoid function

            self.LayerDelta = np.matmul(LayerDel , np.transpose(nextWeight[0:len(nextWeight)-1]))
            self.LayerDelta = self.LayerDelta * self.SigmoidDerivative(y)
            ##print("LayerDelta", self.LayerDelta)
            self.DeltaWeight = np.matmul(np.transpose(self.LayerDelta), self.InputMatrixBias)
            self.DeltaWeight = np.transpose(self.DeltaWeight)
        self.NextWeight = self.LayerWeightMatrix
        ##print("Delta", self.DeltaWeight)
        return self.InputMatrix, self.LayerDelta, self.NextWeight

    ## Updating Layer Weights once the back progated error is found for all layers

    def UpdateLayerWeights(self, Rate):
        self.LayerWeightMatrix = self.LayerWeightMatrix - Rate*self.DeltaWeight

class Neuron():
    def __init__(self, Location, LayerType):
        self.WeightVector = []
        self.InputVector = []
        self.output = None
        self.UpdateWeight = []
        self.delta = None
        self.ActivationOutput = None
        self.Sigma = None
        self.LayerType = LayerType
