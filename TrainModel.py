from OutputModel import MLP
import pickle
from numpy import genfromtxt
import numpy as np

X_TrainData = genfromtxt('train_data.csv', delimiter=',')
y_TrainData = genfromtxt('train_labels.csv', delimiter=',')

## Splitting the Data into Train and Test Data in the ratio 80:20

Divide = int(len(X_TrainData) * 0.8)
X_TraData = X_TrainData[0:Divide]
X_TestData = X_TrainData[Divide + 1:]
y_TraData = y_TrainData[0:Divide]
y_TestData = y_TrainData[Divide + 1:]



## Creating a Model Object Called Model, which instantiates MLP Class.

Model = MLP(InputSize =len(X_TrainData[0]),LearningRate=0.005, Momentum = 0, Epochs=30)

## Adding a hidden Layer with 32 Neurons

Model.AddLayer(25, LayerType= 'hidden')

## Adding an Output layer with the same number of neurons as the number of classes of the data, In our case it is 4

Model.AddLayer(4, LayerType= 'output')

## initializing the Weight Matrices between Input and hidden layer and hidden and output layer

Model.InitializeWeights()

## Training the Model Using the fit Method, For each Epoch the Data is divided into Train and Validation DataSet and the Training stops when the Validation Accuracy starts reducing

Model.fit(X_TraData, y_TraData)

## Testing on the Test Data and calculating the accuracy obtained.

y_pred = Model.Predict(X_TestData)
Accuracy = Model.CalcAccuracy(y_pred, y_TestData)
print("Test Data Accuracy" , Accuracy)

## Storing the Trained Model into a Pickle File

pkl_filename = "FinalModel2.pkl"
with open(pkl_filename, 'wb') as file:
    pickle.dump(Model, file)




