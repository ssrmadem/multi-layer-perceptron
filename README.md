# Multi Layer Perceptron

Implementation of scalable Multi Layer Perceptron from Scratch in python using only numpy library. 
The scalability includes adding multiple hidden layers and choosing various activation functions for hidden layers with softmax activation at the Output Layer.