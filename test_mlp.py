import numpy as np
import pickle
from numpy import genfromtxt
from OutputModel import MLP
STUDENT_NAME = 'SAI SANKEERTH REDDY MADEM'
STUDENT_ID = '20831839'

def test_mlp(data_file):
	# Load the test set
	# START
	X_TestData = genfromtxt(data_file, delimiter=',')
	pkl_filename = "FinalModel2.pkl"
	# Load your network
	# START
	with open(pkl_filename, 'rb') as file:
		pickle_model = pickle.load(file)
	# END

	y_pred = pickle_model.Predict(X_TestData)
	# Predict test set - one-hot encoded
	return y_pred
'''
How we will test your code:

from test_mlp import test_mlp, STUDENT_NAME, STUDENT_ID
from acc_calc import accuracy 

y_pred = test_mlp('./test_data.csv')

test_labels = ...

test_accuracy = accuracy(test_labels, y_pred)*100
'''
